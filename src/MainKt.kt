@file:Suppress("ConstantConditionIf")

import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*
import java.util.function.Consumer

object MainKt {

    private const val DEBUG = false
    private const val LOST_FLAG = -1
    private const val LOST = -2
    private const val ERROR = -3

    private val data: ArrayList<String>
        get() {
            val sc = inputScanner
            val data = ArrayList<String>()
            while (sc.hasNextLine()) {
                data.add(sc.nextLine())
            }
            return data
        }

    private val inputScanner: Scanner
        get() {
            val f = File("D.17.in")
            var `is`: InputStream
            if (f.exists()) {
                try {
                    `is` = FileInputStream(f)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    `is` = System.`in`
                }

            } else
                `is` = System.`in`
            return Scanner(`is`)
        }

    @JvmStatic
    fun main(args: Array<String>) {
        // Check if file exists
        val data = data
        if (data.isEmpty()) {
            if (DEBUG) println("Error. No lines found")
            return
        }

        // Get row and column
        val row: Int
        val column: Int
        val rowCol = data[0].split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        row = Integer.parseInt(rowCol[0])
        column = Integer.parseInt(rowCol[1])
        data.removeAt(0) // These are the directions
        if (DEBUG) data.forEach(Consumer<String> { println(it) })

        // Validate
        if (row < 1 || row > 200) {
            if (DEBUG) println("Too few/many rows")
            return
        }
        if (column < 1 || column > 200) {
            if (DEBUG) println("Too few/many columns")
            return
        }

        val map = Array(row) { CharArray(column) }

        // Add chars to array
        for ((r, s) in data.withIndex()) {
            map[r] = s.toCharArray()
        }

        // Process stuff
        when (val outcome = attemptFlagRetrieval(map)) {
            LOST -> println("Out")
            LOST_FLAG -> println("Lost")
            ERROR -> if (DEBUG) println("An error occurred")
            else -> println(outcome)
        }
    }

    private fun attemptFlagRetrieval(map: Array<CharArray>): Int {
        var steps = 0
        var rowLoc = 0
        var colLoc = 0 // Start at (0,0)
        while (true) {
            try {
                when (map[rowLoc][colLoc]) {
                    'N' -> rowLoc--
                    'S' -> rowLoc++
                    'E' -> colLoc++
                    'W' -> colLoc--
                    'T' -> return steps
                    else -> return ERROR
                }
                steps++
                // Check if we are going on a loop or cannot find flag
                if (steps > map.size * map[0].size) return LOST_FLAG
            } catch (e: ArrayIndexOutOfBoundsException) {
                return LOST
            }
        }
    }
}
