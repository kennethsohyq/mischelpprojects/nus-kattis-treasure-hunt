import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static final boolean DEBUG = false;

    public static void main(String[] args) {
        // Check if file exists
        ArrayList<String> data = getData();
        if (data.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        // Get row and column
        int row, column;
        String[] rowCol = data.get(0).split(" ");
        row = Integer.parseInt(rowCol[0]);
        column = Integer.parseInt(rowCol[1]);
        data.remove(0); // These are the directions
        if (DEBUG) data.forEach(System.out::println);

        // Validate
        if (row < 1 || row > 200) {
            if (DEBUG) System.out.println("Too few/many rows");
            return;
        }
        if (column < 1 || column > 200) {
            if (DEBUG) System.out.println("Too few/many columns");
            return;
        }

        char[][] map = new char[row][column];

        // Add chars to array
        int r = 0;
        for (String s : data) { map[r] = s.toCharArray(); r++; }

        // Process stuff
        int outcome = attemptFlagRetrieval(map);
        switch (outcome) {
            case LOST:
                System.out.println("Out"); break;
            case LOST_FLAG:
                System.out.println("Lost"); break;
            case ERROR:
                if (DEBUG) System.out.println("An error occurred"); break;
            default:
                System.out.println(outcome); break;
        }
    }

    private static int attemptFlagRetrieval(char[][] map) {
        int steps = 0;
        int rowLoc = 0, colLoc = 0; // Start at (0,0)
        while (true) {
            try {
                char loc = map[rowLoc][colLoc];
                switch (loc) {
                    case 'N': rowLoc--; break;
                    case 'S': rowLoc++; break;
                    case 'E': colLoc++; break;
                    case 'W': colLoc--; break;
                    case 'T': return steps;
                    default: return ERROR;
                }
                steps++;
                // Check if we are going on a loop or cannot find flag
                if (steps > (map.length * map[0].length)) return LOST_FLAG;
            } catch (ArrayIndexOutOfBoundsException e) {
                return LOST;
            }
        }
    }

    private static final int LOST_FLAG = -1, LOST = -2, ERROR = -3;

    private static ArrayList<String> getData() {
        Scanner sc = getInputScanner();
        ArrayList<String> data = new ArrayList<>();
        while (sc.hasNextLine()) {
            data.add(sc.nextLine());
        }
        return data;
    }

    private static Scanner getInputScanner() {
        File f = new File("D.17.in");
        InputStream is;
        if (f.exists()) {
            try {
                is = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                is = System.in;
            }
        }
        else is = System.in;
        return new Scanner(is);
    }
}
